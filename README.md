# rhelloworld package

This is an R package for printing 'hello world!' to the R console.

## Dependencies
You need to have `devtools` installed to use this package. Install devtools with `install.packages('devtools')`

## How to install
Install this package from Gitlab by running `devtools::install_gitlab('jamesazam/rhelloworld/rhelloworld')`

## List of functions
1. `helloworld()` - this function takes in no inputs and prints 'hello world!' to the R console 

